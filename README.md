# AllegianceOS

Allegiance OS is a Linux/GNU distribution based on Slackware Linux. 

It is currently 64-bit only, the isos are hosted at: www.sourceforge.net/projects/allegianceos

This repository will house the changes moving forward as Python3 scripts are converted to Go binaries. 

Questions, queries, concerns & ideas may be sent to kevan[dot]reifel[at]gmail[dot]com.



Slackware is a registered trademark of Patrick Volkerding.
No partnership written or otherwise exists between this	project	and Slackware.
All software comes as-is, use at your own risk.


Note: unless otherwise stated, all software here is licensed under the GPLv3.
