package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"strings"
	"time"
	//"io"
	//"runtime"
)

func CheckRoot() {
	uid := os.Geteuid()
	if uid == 0 {
		//fmt.Println("-> Info: Program running as root, proceeding..")
	} else {
		fmt.Println("-> Fatal Error!: Program must be run as root!")
		os.Exit(1)
	}
}

func PackagePrinter(pkg PrintPackage) {
	pkg.print()
}

func MultiPackPrinter(pkg []PrintPackage) {
	for i := 1; i < len(pkg); i++ {
		pkg[i].print()
	}
}

func SearchSlackPackage(pkg string) (int, []SlackwarePackageDetails) {
	var SlaptResponse SlackPackageDetails
	command := "aos-searchslack " + pkg
	output := RunCommand(command)
	json.Unmarshal([]byte(output), &SlaptResponse)
	results := SlaptResponse.Results
	length := len(results)
	if length > 0 {
		return length, results
	} else {
		return 0, results
	}
}

func SearchSlackBuild(pkg string) (int, []SlackBuildDetails) {
	var SlackBuildResponse SlackBuildSearchResult
	command := "aos-searchsbo -j " + pkg
	output := RunCommand(command)
	json.Unmarshal([]byte(output), &SlackBuildResponse)
	results := SlackBuildResponse.Results
	length := len(results)
	if length > 0 {
		return length, results
	} else {
		return 0, results
	}
}

func RemoveDuplicates(array []PackageDetails, indices []int) ([]PackageDetails, bool) {
	length := len(indices)
	if length > 1 {
		newArray := remove(array, indices[0])
		if length == 2 {
			return newArray, true
		} else {
			return newArray, false
		}
	} else {
		return array, true
	}
}

func RemovePackageByName(pkgs []PackageDetails, pkg string) []PackageDetails {
	for i := 0; i < len(pkgs); i++ {
		if pkgs[i].Name == pkg {
			array := remove(pkgs, i)
			return array
		}
	}
	return pkgs
}

func GetPackageNames(packages []PackageDetails) []string {
	var packageNames []string
	for i := 0; i < len(packages); i++ {
		if !StringArrayContains(packageNames, packages[i].Name) {
			packageNames = append(packageNames, packages[i].Name)
		}
	}
	return packageNames
}

func GetSBODepFilePath(pkg string) string {
	filepath := Config.PathToSBOQueueFiles + pkg + Config.SBOQueueFileExtension
	return filepath
}

func GetAOSDepFilePath(pkg string) string {
	filepath := Config.PathToAOSQueueFiles + pkg + Config.AOSQueueFileExtension
	return filepath
}

func GenerateSBODepFile(pkg string) {
	command := "sqg -p " + pkg
	output := RunCommand(command)
	if Config.VeryVerbose {
		fmt.Println(output)
	}
}

func RemoveSBODepFile(filepath string) {
	command := "rm " + filepath
	output := RunCommand(command)
	if Config.Verbose {
		fmt.Println(output)
	}
}

func GetDepFile(pkg string) (bool, []string) {
	GenerateSBODepFile(pkg)
	AOSDepFilePath := GetAOSDepFilePath(pkg)
	SBODepFilePath := GetSBODepFilePath(pkg)
	AOSDepFileExists := FileExists(AOSDepFilePath)
	SBODepFileExists := FileExists(SBODepFilePath)
	if AOSDepFileExists {
		return true, ReadFileIntoArray(AOSDepFilePath)
	} else {
		if SBODepFileExists {
			sboFile := ReadFileIntoArray(SBODepFilePath)
			RemoveSBODepFile(SBODepFilePath)
			return true, sboFile
		} else {
			var empty []string
			return false, empty
		}
	}
}

func CleanString(s string) string {
	s = strings.TrimSpace(s)
	for i := 0; i < len(DisallowedCharacters); i++ {
		s = strings.Replace(s, DisallowedCharacters[i], "", -1)
	}
	return s
}

func StringToPackageDetails(pkg string, status string) PackageDetails {
	var p PackageDetails
	pkgArray := strings.Split(pkg, "-")
	length := len(pkgArray)
	p.PackagePath = Config.PathToPackages + pkg
	if length < 2 {
		p.Name = pkgArray[0]
	}
	if (length == 2) || (length == 3) {
		p.Name = pkgArray[0]
		p.Version = pkgArray[1]
	}
	if length == 4 {
		p.Name = pkgArray[0]
		p.Version = pkgArray[1]
		p.Arch = pkgArray[2]
		p.Build = pkgArray[3]
	}
	if length >= 5 {
		iBuild := length - 1
		iVersion := length - 3
		iArch := length - 2
		iNameEnd := length - 3
		nameArray := pkgArray[0:iNameEnd]
		var fullName string
		for i := 0; i < len(nameArray); i++ {
			if i == len(nameArray)-1 {
				fullName = fullName + nameArray[i]
			} else {
				fullName = fullName + nameArray[i] + "-"
			}
		}
		p.Name = fullName
		p.Build = pkgArray[iBuild]
		p.Version = pkgArray[iVersion]
		p.Arch = pkgArray[iArch]
	}
	if status == "Installed." {
		p.Installed = true
	}
	if status == "Upgraded." {
		p.Upgrade = true
	}
	if status == "Removed." {
		p.Removed = true
	}
	if status == "Added." {
		p.Added = true
	}
	if status == "Rebuilt." {
		p.Rebuilt = true
	}
	return p
}

func GetInstallStatus(pkg string, method string) bool {
	if method == "package" {
		return false
	} else {
		if method == "slackbuild" {
			return false
		} else {
			return false
		}
	}
}

func StringArrayContains(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}
	return false
}

func remove(slice []PackageDetails, s int) []PackageDetails {
	return append(slice[:s], slice[s+1:]...)
}

func LoadConfiguration() {
	Config.PathToSBOQueueFiles = "/var/lib/sbopkg/queues/"
	Config.PathToAOSQueueFiles = "/usr/share/aos/queues/"
	Config.PathToAOSBuildFiles = "/usr/share/aos/builds/"
	Config.PathToInstallLogs = "/var/log/aos/installer/"
	Config.PathToSBODirectory = "/var/lib/sbopkg/SBo/14.2/"
	Config.PathToPackages = "/var/log/packages/"
	Config.SBOQueueFileExtension = ".sqf"
	Config.AOSQueueFileExtension = ".aqf"
	Config.DominateInstaller = "slackbuild"
	Config.Verbose = false
	Config.VeryVerbose = false
	Config.ServerMode = false
	Config.UpdateLogInterval = 12
	if !Config.ServerMode {
		if Config.Verbose {
			fmt.Println("-> Loaded configuration..")
		}
	}
}

func ValidateFolderPaths() bool {
	validPathToSBOQueueFiles := CheckAndCreatePath(Config.PathToSBOQueueFiles)
	if !validPathToSBOQueueFiles {
		fmt.Fprintf(os.Stderr, "-> Fatal Error! Unable to create path: %v\n", Config.PathToAOSQueueFiles)
		os.Exit(1)
	}
	validPathToAOSQueueFiles := CheckAndCreatePath(Config.PathToAOSQueueFiles)
	if !validPathToAOSQueueFiles {
		fmt.Fprintf(os.Stderr, "-> Fatal Error! Unable to create path: %v\n", Config.PathToAOSQueueFiles)
		os.Exit(1)
	}
	validPathToAOSBuildFiles := CheckAndCreatePath(Config.PathToAOSBuildFiles)
	if !validPathToAOSBuildFiles {
		fmt.Fprintf(os.Stderr, "-> Fatal Error! Unable to create path: %v\n", Config.PathToAOSBuildFiles)
		os.Exit(1)
	}
	validPathToInstallLogs := CheckAndCreatePath(Config.PathToInstallLogs)
	if !validPathToInstallLogs {
		fmt.Fprintf(os.Stderr, "-> Fatal Error! Unable to create path: %v\n", Config.PathToInstallLogs)
		os.Exit(1)
	}
	validPathToSBO := CheckAndCreatePath(Config.PathToSBODirectory)
	if !validPathToSBO {
		fmt.Fprintf(os.Stderr, "-> Fatal Error! Unable to create path: %v\n", Config.PathToSBODirectory)
		os.Exit(1)
	}
	return true
}

func RunCommand(command string) string {
	cmdArgs := strings.Fields(command)
	cmd, err := exec.Command(cmdArgs[0], cmdArgs[1:]...).Output()
	if err != nil {
		//fmt.Println(cmdArgs)
		return "Error occurred running command: '" + command + "'."
	}
	return string(cmd)
}

func ReadFileIntoArray(filepath string) []string {
	var FileArray []string
	file, err := os.Open(filepath)
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		if !strings.Contains(scanner.Text(), "#") {
			//mt.Println(scanner.Text())
			FileArray = append(FileArray, scanner.Text())
		}
	}
	//fmt.Println(FileArray)
	return FileArray
}

func RunCommandReturnArray(command string) []string {
	rawOutput := RunCommand(command)
	outputArray := strings.Split(rawOutput, "\n")
	return outputArray
}

func UpdateIsNeeded() bool {
	now := time.Now()
	if !FileExists("/usr/share/aos/lastPackageUpdate") {
		today := now.Format(time.RFC822)
		makeFile := CreateFile("/usr/share/aos/lastPackageUpdate", today)
		if !makeFile {
			fmt.Println("-> Fatal error! Was unable to create a package update file.")
			os.Exit(1)
		}
	}
	lastDateString := RunCommand("cat /usr/share/aos/lastPackageUpdate")
	lastDate, _ := time.Parse(time.RFC822, lastDateString)
	diff := now.Sub(lastDate)
	hrs := int(diff.Hours())
	if hrs >= Config.UpdateLogInterval {
		return true
	} else {
		return false
	}
}

func SBOUpdateIsNeeded() bool {
	now := time.Now()
	if !FileExists("/usr/share/aos/lastSlackBuildUpdate") {
		today := now.Format(time.RFC822)
		makeFile := CreateFile("/usr/share/aos/lastSlackBuildUpdate", today)
		if !makeFile {
			fmt.Println("-> Fatal error! Was unable to create a package update file.")
			os.Exit(1)
		}
	}
	lastDateString := RunCommand("cat /usr/share/aos/lastSlackBuildUpdate")
	lastDate, _ := time.Parse(time.RFC822, lastDateString)
	diff := now.Sub(lastDate)
	hrs := int(diff.Hours())
	if hrs >= Config.UpdateLogInterval {
		return true
	} else {
		return false
	}
}

func UpdatePackageUpgradeTime() {
	now := time.Now()
	today := now.Format(time.RFC822)
	err := os.Remove("/usr/share/aos/lastPackageUpdate")
	if err != nil {
		fmt.Println("-> Info: Error occurred removing previous package update file.")
		os.Exit(1)
	} else {
		makeFile := CreateFile("/usr/share/aos/lastPackageUpdate", today)
		if !makeFile {
			fmt.Println("-> Fatal error! Was unable to create a package update file.")
			os.Exit(1)
		}
	}
}

func UpdateSlackBuildUpgradeTime() {
	now := time.Now()
	today := now.Format(time.RFC822)
	err := os.Remove("/usr/share/aos/lastSlackBuildUpdate")
	if err != nil {
		fmt.Println("-> Info: Error occurred removing previous package update file.")
		os.Exit(1)
	} else {
		makeFile := CreateFile("/usr/share/aos/lastSlackBuildUpdate", today)
		if !makeFile {
			fmt.Println("-> Fatal error! Was unable to create a slackbuild update file.")
			os.Exit(1)
		}
	}
}

func CheckSlackPkgLock() bool {
	if !Config.ServerMode {
		if Config.Verbose {
			fmt.Println("-> Info: Checking for slackpkg lock files..")
		}
	}
	path := "/var/lock/"
	locks := RunCommandReturnArray("ls /var/lock/")
	lockCount := len(locks)
	for i := 0; i < lockCount; i++ {
		if strings.Contains(locks[i], "slackpkg") {
			fullPath := path + locks[i]
			err := os.Remove(fullPath)
			if err != nil {
				if !Config.ServerMode {
					if Config.Verbose {
						fmt.Println("-> Info: slackpkg lock file not removed. This will prevent expected behavior when updating.")
					}
				}
				return true
			} else {
				if !Config.ServerMode {
					if Config.Verbose {
						fmt.Println("-> Info: a slackpkg lock file was removed.")
					}
				}
				return false
			}
		}
	}
	return false
}

func CheckSBOLock() bool {
	lockEnabled := FileExists("/var/run/sbopkg.pid")
	if lockEnabled {
		err := os.Remove("/var/run/sbopkg.pid")
		if err != nil {
			if !Config.ServerMode {
				if Config.Verbose {
					fmt.Println("-> Info: sbopkg lock file not removed. This will prevent expected behavior when updating.")
				}
			}
			return true
		} else {
			if !Config.ServerMode {
				if Config.Verbose {
					fmt.Println("-> Info: a sbopkg lock file was removed.")
				}
			}
			return false
		}
	}
	return false
}

func FileExists(filepath string) bool {
	_, err := os.Stat(filepath)
	if err != nil {
		return false
	} else {
		return true
	}
}

func CreateFile(file string, data string) bool {
	f, err := os.Create(file)
	if err != nil {
		f.Close()
		return false
	} else {
		binary := []byte(data)
		_, err := f.Write(binary)
		if err != nil {
			return false
			f.Close()
		}
		f.Close()
		return true
	}
}

func CreateDir(dir string) bool {
	err := os.Mkdir(dir, 700)
	if err != nil {
		return false
	} else {
		return true
	}
}

func CreatePath(path string) bool {
	err := os.MkdirAll(path, 700)
	if err != nil {
		return false
	} else {
		return true
	}
}

func CheckAndCreateDir(dir string) bool {
	exists := PathExists(dir)
	if exists {
		return true
	} else {
		create := CreateDir(dir)
		if !create {
			return false
		}
		return true
	}

}

func CheckAndCreatePath(path string) bool {
	exists := PathExists(path)
	if exists {
		return true
	} else {
		create := CreatePath(path)
		if !create {
			return false
		}
		return true
	}
}

func PathExists(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	return false
}
