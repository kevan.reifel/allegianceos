package main

import (
	"fmt"
	"strings"
	"time"
)

func GetPackageFromArrayByName(name string, array []PackageDetails) (PackageDetails, bool) {
	var empty PackageDetails
	length := len(array)
	for i := 0; i < length; i++ {
		if array[i].Name == name {
			return array[i], true
		}
	}
	return empty, false
}

func FindPackageChanges() {
	if !Config.ServerMode {
		fmt.Println("-> Sorting package updates..")
	}
	updates := State.ChangedPackages
	updateCount := len(updates)
	var returnObject PackageChangeResponse
	var testStrings []string
	for i := 0; i < updateCount; i++ {
		updatePkg := updates[i]
		var contains bool
		if updatePkg.Name == "packages" {
			contains = true
		} else {
			contains = false
		}
		if !contains {
			testStrings = append(testStrings, updatePkg.Name)
			installedPkg, installed := GetInstalledPackage(updatePkg.Name)
			if !installed {
				updatePkg.Source = "package"
				if updatePkg.Added {
					returnObject.Additions = append(returnObject.Additions, updatePkg)
					if !Config.ServerMode {
						if Config.Verbose {
							if Config.VeryVerbose {
								fmt.Println("\n", updatePkg.Name, "was added to the package repository.")
								fmt.Println("Package Details: ", updatePkg)
							}
						}
					}
				} else {
					if !Config.ServerMode {
						if Config.Verbose {
							if Config.VeryVerbose {
								fmt.Println("\n", updatePkg.Name, " is not currently installed but an update exists.")
								fmt.Println("Package Details: ", updatePkg)
							}
						}
					}
				}
			} else {
				updatePkg.PreviousVersion = installedPkg.Version
				updatePkg.PreviousBuild = installedPkg.Build
				updatePkg.Source = "package"
				if updatePkg.Rebuilt {
					returnObject.Rebuilds = append(returnObject.Rebuilds, updatePkg)
					if !Config.ServerMode {
						if Config.Verbose {
							if Config.VeryVerbose {
								fmt.Println("\n", updatePkg.Name, "was rebuilt.")
								fmt.Println("Package Details: ", updatePkg)
							}
						}
					}
				}
				if updatePkg.Removed {
					returnObject.Removals = append(returnObject.Removals, updatePkg)
					if !Config.ServerMode {
						if Config.Verbose {
							if Config.VeryVerbose {
								fmt.Println("\n", updatePkg.Name, "was removed.")
								fmt.Println("Package Details: ", updatePkg)
							}
						}
					}
				}
				if updatePkg.Upgrade {
					returnObject.Upgrades = append(returnObject.Upgrades, updatePkg)
					if !Config.ServerMode {
						if Config.Verbose {
							if Config.VeryVerbose {
								fmt.Println("\n", updatePkg.Name, "was upgraded.")
								fmt.Println("Package Details: ", updatePkg)
							}
						}
					}
				}
			}
		}
	}
	State.ProcessingPackageUpdates = false
	UpgradeResponse = returnObject
}

func CheckPackageInstallStatus() {
	currentList := State.Current
	var processing []PackageDetails
	for i := 0; i < len(currentList); i++ {
		pkg := currentList[i]
		name := pkg.Name
		method := pkg.Source
		pkg.Installed = GetInstallStatus(name, method)
		processing = append(processing, pkg)
	}
	State.Current = processing
}

func SearchForOnePackage(pkg string) bool {
	count, results := SearchSlackPackage(pkg)
	if count <= 0 {
		return false
	} else {
		for i := 0; i < count; i++ {
			if pkg == results[i].Name {
				return true
			}
		}
		return false
	}
}

func SearchForOneSlackBuild(pkg string) bool {
	count, results := SearchSlackBuild(pkg)
	if count <= 0 {
		return false
	} else {
		for i := 0; i < count; i++ {
			if pkg == results[i].Name {
				return true
			}
		}
		return false
	}
}

func CreateSBOList() {
	installed := State.Installed
	length := len(installed)
	var SBo []PackageDetails
	for i := 0; i < length; i++ {
		pkg := installed[i]
		if strings.Contains(pkg.Build, "SBo") {
			SBo = append(SBo, pkg)
		}
	}
	State.SBo = SBo
}

func ProcessSBOUpdates() {
	if !Config.ServerMode {
		fmt.Println("-> Fetching SlackBuild updates..")
	}
	if SBOUpdateIsNeeded() {
		update := RunCommand("sbopkg -r")
		if !Config.ServerMode {
			if Config.Verbose {
				if Config.VeryVerbose {
					fmt.Println(update)
				}
			}
		}
	} else {
		fmt.Println("-> Info: SBO was recently updated, skipping.")
	}
	State.RunningSBOpkg = true
	command := "sbopkg -c"
	output := strings.TrimSpace(RunCommand(command))
	if !Config.ServerMode {
		fmt.Println("-> Done!")
	}
	outArray := strings.Split(output, "\n")
	length := len(outArray)
	if !Config.ServerMode {
		fmt.Println("-> Parsing update info..")
	}
	for i := 0; i < length; i++ {
		if State.ProcessingSBOUpdates {
			if strings.Contains(outArray[i], "POTENTIAL UPDATE") {
				installedI := i + 1
				updateI := i + 2
				packageInstalled := strings.Split(outArray[installedI], ":")[1]
				packageUpdate := strings.Split(outArray[updateI], ":")[1]
				installed := StringToPackageDetails(packageInstalled, "Installed.")
				update := StringToPackageDetails(packageUpdate, "Upgraded.")
				update.PreviousBuild = installed.Build
				update.PreviousVersion = installed.Version
				update.Source = "slackbuild"
				UpgradeResponse.Upgrades = append(UpgradeResponse.Upgrades, update)
			}
		}
		if strings.Contains(outArray[i], "potential updates...") {
			State.ProcessingSBOUpdates = true
		}
	}
	if !Config.ServerMode {
		fmt.Println("-> SlackBuild update data processed!")
	}
	State.RunningSBOpkg = false
	State.ProcessingSBOUpdates = false
}

func ProcessSlackwarePackageUpgrades() {
	GetChangelogDatesFromAOS()
	UpdateSlackPackageChangelogs()
	GetChangelogDatesFromSlackpkg()
	GetUpdatedChangelogs()
	GetPackagesFromChangelog()
	FindPackageChanges()
	if !Config.ServerMode {
		fmt.Println("-> Completed processing package updates!")
	}
}

func ProcessInstalledPackages() {
	cmd := "ls" + Config.PathToPackages
	installedPackages := RunCommandReturnArray(cmd)
	var processing []PackageDetails
	for i := 0; i < len(installedPackages); i++ {
		var p PackageDetails
		pkg := installedPackages[i]
		pkgArray := strings.Split(pkg, "-")
		length := len(pkgArray)
		p.PackagePath = Config.PathToPackages + pkg
		if length < 2 {
			p.Name = pkgArray[0]
			p.Installed = true
			processing = append(processing, p)
		}
		if (length == 2) || (length == 3) {
			p.Name = pkgArray[0]
			p.Version = pkgArray[1]
			p.Installed = true
			processing = append(processing, p)
		}
		if length == 4 {
			p.Name = pkgArray[0]
			p.Version = pkgArray[1]
			p.Arch = pkgArray[2]
			p.Build = pkgArray[3]
			p.Installed = true
			processing = append(processing, p)
		}
		if length >= 5 {
			iBuild := length - 1
			iVersion := length - 3
			iArch := length - 2
			iNameEnd := length - 3
			nameArray := pkgArray[0:iNameEnd]
			var fullName string
			for i := 0; i < len(nameArray); i++ {
				if i == len(nameArray)-1 {
					fullName = fullName + nameArray[i]
				} else {
					fullName = fullName + nameArray[i] + "-"
				}
			}
			p.Name = fullName
			p.Build = pkgArray[iBuild]
			p.Version = pkgArray[iVersion]
			p.Arch = pkgArray[iArch]
			p.Installed = true
			processing = append(processing, p)
		}
	}
	State.Installed = processing
}

func ProcessUpdates(packages bool, slackbuilds bool) {
	if packages {
		ProcessInstalledPackages()
		ProcessSlackwarePackageUpgrades()
	}
	if slackbuilds {
		ProcessSBOUpdates()
	}
	CollapseCurrent()
	CollapseAdditions()
	CollapseDowngrades()
	CollapseRebuilds()
	CollapseRemovals()
	CollapseUpgrades()
	if !Config.ServerMode {
		if Config.Verbose {
			fmt.Println("Total Upgrades:", len(UpgradeResponse.Upgrades), "\nTotal Downgrades:", len(UpgradeResponse.Downgrades), "\nTotal Additions:", len(UpgradeResponse.Additions), "\nTotal Rebuilds:", len(UpgradeResponse.Rebuilds), "\nTotal Removals: ", len(UpgradeResponse.Removals))
		}
	}
}

func InstallUpdates() {
	UpgradePackages(UpgradeResponse.Upgrades)
}

func InstallDowngrades() {
	DowngradePackages(UpgradeResponse.Downgrades)
}

func UpdateSlackPackageChangelogs() {
	if !Config.ServerMode {
		fmt.Println("-> Attempting to update slackpkg changelogs..")
	}
	updateRequired := UpdateIsNeeded()
	if updateRequired {
		_ = RunCommand("slackpkg update gpg")
		_ = RunCommand("slackpkg update")
		UpdatePackageUpgradeTime()
	} else {
		if !Config.ServerMode {
			fmt.Println("-> Info: Skipped update of changelogs, time hasn't exceeded configuration.")
		}
	}
	if !Config.ServerMode {
		fmt.Println("-> Done!")
	}
	//_ = RunCommand("rm -R /var/lib/aos/installer/changelogs")
	//_ = RunCommand("cp -R /var/lib/slackpkg/ChangeLogs /var/lib/aos/installer/changelogs")
}

func GetChangelogDatesFromAOS() {
	State.ProcessingPackageUpdates = true
	if !Config.ServerMode {
		fmt.Println("-> Processing package mirrors..")
	}
	changelogs := RunCommandReturnArray("ls /var/lib/aos/installer/changelogs/")
	length := len(changelogs)
	for i := 0; i < length; i++ {
		if !strings.Contains(changelogs[i], ".idx") {
			command := "head -1" + " /var/lib/aos/installer/changelogs/" + changelogs[i]
			dateString := RunCommand(command)
			if dateString != "" {
				dateString := strings.Replace(dateString, "\n", "", -1)
				if !strings.Contains(dateString, "Error occurred") {
					var logData ChangelogData
					lastTime, err := time.Parse("Mon Jan _2 15:04:05 MST 2006", dateString)
					if err != nil {
						if !Config.ServerMode {
							fmt.Println("\nError:\n", err, "\n", dateString)
						}
					}
					logData.FileName = changelogs[i]
					logData.FilePath = "/var/lib/slackpkg/ChangeLogs/"
					logData.Date = lastTime.Unix()
					logData.DateString = dateString
					logData.Updated = false
					ChangeLogs = append(ChangeLogs, logData)
				}
			}
		}
	}
}

func GetChangelogDatesFromSlackpkg() {
	changelogs := RunCommandReturnArray("ls /var/lib/slackpkg/ChangeLogs/")
	length := len(changelogs)
	for i := 0; i < length; i++ {
		if !strings.Contains(changelogs[i], ".idx") {
			command := "head -1" + " /var/lib/slackpkg/ChangeLogs/" + changelogs[i]
			dateString := RunCommand(command)
			if dateString != "" {
				dateString := strings.Replace(dateString, "\n", "", -1)
				if !strings.Contains(dateString, "Error occurred") {
					var logData ChangelogData
					lastTime, err := time.Parse("Mon Jan _2 15:04:05 MST 2006", dateString)
					if err != nil {
						if !Config.ServerMode {
							fmt.Println("\nError:\n", err, "\n", dateString)
						}
					}
					logData.FileName = changelogs[i]
					logData.FilePath = "/var/lib/slackpkg/ChangeLogs/"
					logData.Date = lastTime.Unix()
					logData.DateString = dateString
					logData.Updated = true
					ChangeLogs = append(ChangeLogs, logData)
				}
			}
		}
	}
}

func FetchUpdatedChangelogData(name string) (bool, ChangelogData) {
	complete := ChangeLogs
	length := len(complete)
	var empty ChangelogData
	for i := 0; i < length; i++ {
		if (complete[i].FileName == name) && complete[i].Updated {
			return true, complete[i]
		}
	}
	return false, empty
}

func GetUpdatedChangelogs() {
	complete := ChangeLogs
	length := len(complete)
	var updated []ChangelogData
	for i := 0; i < length; i++ {
		if !complete[i].Updated {
			updatedLog, logData := FetchUpdatedChangelogData(complete[i].FileName)
			if updatedLog {
				if logData.Date > complete[i].Date {
					logData.OldDateString = complete[i].DateString
					updated = append(updated, logData)
				}
			}
		}
	}
	UpdatedChangeLogs = updated
}

func ParseChangeLogLine(line string) ([]string, string) {
	var emptyArray []string
	var empty string
	if strings.Contains(line, ":") {
		split := strings.Split(line, "/")
		if strings.Contains(split[1], ".txz") {
			fmt.Println(split[1], split[2])
		}

	}
	return emptyArray, empty
}

func GetPackagesFromChangelog() {
	if !Config.ServerMode {
		fmt.Println("-> Building list of changed packages..")
	}
	logs := UpdatedChangeLogs
	length := len(logs)
	for i := 0; i < length; i++ {
		filePath := logs[i].FilePath + logs[i].FileName
		command := "cat " + filePath
		file := RunCommand(command)
		fileArray := strings.Split(file, "\n")
		fileLength := len(fileArray)
		var updated []string
		for f := 0; f < fileLength; f++ {
			if fileArray[f] == logs[i].OldDateString {
				break
			} else {
				if strings.Contains(fileArray[f], ".txz") {
					//fmt.Println(fileArray[f])
					updated = append(updated, fileArray[f])
				}
			}
		}
		for c := 0; c < len(updated); c++ {
			firstArray := strings.Split(updated[c], ":")
			secondArray := strings.Split(firstArray[0], "/")
			thirdArray := strings.Split(secondArray[1], ".txz")
			status := strings.TrimSpace(firstArray[1])
			pkg := thirdArray[0]
			details := StringToPackageDetails(pkg, status)
			State.ChangedPackages = append(State.ChangedPackages, details)
		}
		//fmt.Println(State.ChangedPackages)
	}
}

func GetInstalledPackage(name string) (PackageDetails, bool) {
	var empty PackageDetails
	installed := State.Installed
	length := len(installed)
	for i := 0; i < length; i++ {
		if installed[i].Name == name {
			return installed[i], true
		}
	}
	return empty, false
}
