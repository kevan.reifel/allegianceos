package main

import "fmt"

type StateObject struct {
	ProcessingInstalls        bool
	ProcessingPackageUpdates  bool
	ProcessingSBOUpdates      bool
	RunningInstalls           bool
	RunningPackageUpdates     bool
	RunningSBOUpdates         bool
	DownloadingNewPackages    bool
	DownloadingPackageUpdates bool
	DownloadingSBOUpdates     bool
	RunningSBOpkg             bool
	CurrentPackage            PackageDetails
	CurrentQueue              []PackageDetails
	CompletedPackages         []PackageDetails
	Current                   []PackageDetails
	Selected                  []PackageDetails
	Processed                 []PackageDetails
	Errored                   []PackageDetails
	Completed                 []PackageDetails
	Installed                 []PackageDetails
	ChangedPackages           []PackageDetails
	SBo                       []PackageDetails
	SBoUpdates                []PackageDetails
}

type Configuration struct {
	Verbose               bool   `json:"verbose"`
	VeryVerbose           bool   `json:"very_verbose"`
	ServerMode            bool   `json:"server_mode"`
	DominateInstaller     string `json:"primary_installation_method"`
	PathToSBOQueueFiles   string `json:"path_to_sbo_queue_files"`
	PathToAOSQueueFiles   string `json:"path_to_aos_queue_files"`
	PathToAOSBuildFiles   string `json:"path_to_aos_build_files"`
	PathToSBODirectory    string `json:"path_to_sbo_directory"`
	PathToInstallLogs     string `json:"path_to_install_logs"`
	PathToPackages        string `json:"path_to_package_directory"`
	PackageInstaller      string `json:"package_installer_exec"`
	SlackBuildInstaller   string `json:"slackbuild_installer_exec"`
	SBOQueueFileExtension string `json:"sbo_queue_file_extension"`
	AOSQueueFileExtension string `json:"aos_queue_file_extension"`
	AOSBuildFileExtension string `json:"aos_build_file_extension"`
	UpdateLogInterval     int    `json:"update_log_interval"`
}

/*
{
	// Run program in verbose mode (only applies if not running in server mode):
	"verbose":true,

	// Run program in very verbose mode (only applies if not running in server mode):
	"very_verbose":false,

	// Run program in server mode for local webclient (disables text output, runs localhost-only REST server):
	"server_mode":false,

	// The preferred install method (if both a 'package' and 'slackbuild' are available, this will take precidence):
	"primary_installation_method":"package", // (MUST be 'package' or 'slackbuild'!)

	// Path to SBO queue files:
	"path_to_sbo_queue_files":"/var/lib/sbopkg/queues/",

	// Path to AOS queue files (may include names for both packages and slackbuilds):
	"path_to_aos_queue_files":"/usr/share/aos/queues/",

	// Path to AOS build files (adds on build options to an sbopkg install, for example: "GTK3=yes ./transmission.SlackBuild"):
	"path_to_aos_build_files":"/usr/share/aos/builds/",

	// Path to SBO tree directory (should be updated if using different version):
	"path_to_sbo_directory":"/var/lib/sbopkg/SBo/14.2/",

	// Path to AOS installer logs:
	"path_to_install_logs":"/var/log/aos/installer/",

	// Path to package directory (default /var/log/packages)
	"path_to_package_directory":"/var/log/packages/",

	// Time (in hours) to cache old update files before updating changelogs and package lists:
	"update_log_interval":12,

	//
	// NOTE: It's best if the options after this remain default!
	//
	"package_installer_exec":"slapt-get",
	"slackbuild_installer_exec":"sbopkg",
	"sbo_queue_file_extension":".sqf",
	"aos_queue_file_extension":".aqf",
	"aos_build_file_extension":".abf",
}
*/

// this is for responding to a REST call for 'upgrades'
type PackageChangeResponse struct {
	Upgrades   []PackageDetails `json:"upgrades"`
	Rebuilds   []PackageDetails `json:"rebuilds"`
	Additions  []PackageDetails `json:"additions"`
	Removals   []PackageDetails `json:"removals"`
	Downgrades []PackageDetails `json:"downgrades"`
}

type ChangelogData struct {
	FileName      string
	FilePath      string
	Date          int64
	DateString    string
	Updated       bool
	OldDateString string
}

type PackageToInstall struct {
	Name   string   `json:"package_name"`
	Method string   `json:"source"`
	Status string   `json:"status"`
	Output []string `json:"output"`
}

type FuncResponse struct {
	Raw  string
	JSON string
}

type ProcessedPackage struct {
	Name         string
	Method       string // script/sbo/slapt
	BuildOptions string
	RawOut       []string
	JSONOut      string
}

type PrintPackage interface {
	print()
}

func (pd SlackwarePackageDetails) print() {
	fmt.Println("Name:", pd.Name, "\nVersion:", pd.Version, "\nBuild:", pd.Build, "\nStatus:", pd.Status, "\nDescription:", pd.Description, "\n")
}

func (pd SlackBuildDetails) print() {
	fmt.Println("Name:", pd.Name, "\nVersion:", pd.Version, "\nDescription:", pd.Description, "\nRequires:", pd.Requires, "\n")
}

// The data retrieved by the Slackware package search:
type SlackwarePackageDetails struct {
	Name        string `json:"name"`
	Version     string `json:"version"`
	Build       string `json:"build"`
	Arch        string `json:"arch"`
	Description string `json:"description"`
	Status      string `json:"status"`
	Source      string `json:"source"`
}

// The data retrieved by the SBO search:
type SlackBuildDetails struct {
	Name             string   `json:"name"`
	SmallDescription string   `json:"smdesc"`
	Description      string   `json:"description"`
	HomePage         string   `json:"homepage"`
	Source           string   `json:"source"`
	Version          string   `json:"version"`
	Requires         []string `json:"requires"`
	Maintainer       string   `json:"maintainer"`
}

type PackageDetails struct {
	Name            string            `json:"name"`
	Arch            string            `json:"arch"`
	Build           string            `json:"build"`
	PreviousBuild   string            `json:"previous_build"`
	Version         string            `json:"version"`
	PreviousVersion string            `json:"previous_version"`
	Description     string            `json:"description"`
	Source          string            `json:"source"`
	Installed       bool              `json:"installed"`
	Upgrade         bool              `json:"upgrade"`
	Rebuilt         bool              `json:"rebuilt"`
	Removed         bool              `json:"removed"`
	Added           bool              `json:"added"`
	Meta            map[string]string `json:"meta"`
	PackagePath     string            `json:"package_path"`
}

type SlackPackageDetails struct {
	Results []SlackwarePackageDetails `json:"results"`
}

type SlackBuildSearchResult struct {
	Results []SlackBuildDetails `json:"results"`
}

// The data retrieved by the NPM search:
/*
type NodeModuleDetails struct {
	Name        string   `json:"name"`
	Scope       string   `json:"scope"`
	Version     string   `json:"version"`
	Description string   `json:"description"`
	Keywords    []string `json:"keywords"`
	Date        string   `json:"date"`
	Links       string   `json:"links"`
	Author      string   `json:"author"`
	Publisher   string   `json:"publisher"`
	Maintainers []string `json:"maintainers"`
	Source      string   `json:"source"`
}*/

// The format data is sent to the UI in:

/*type NodeSearchResult struct {
	Results []NodeModuleDetails `json:"results"`
}*/
