This is 'asc' or Allegiance Software Control.

Currently it only serves as a drop-in replacement for aos-installer, the Python3 version currently in older AllegianceOS releases.

Other features will be converted from Python3 and be added to this as time goes on. Check back for updates!

Please excuse the format/structure, it's (slowly) being cleaned up.

To build, use: 

"go mod download"
"go build *.go"

Slackware 15.0 is required. 

In addition, sbopkg, slapt-get and slackpkg are needed for full functionality.



Slackware is a registered trademark of Patrick Volkerding.
No partnership written or otherwise exists between this	project	and Slackware.
All software comes as-is, use at your own risk.




Program options (some implemented, some partially, quite a few still to be converted):


	Package Management:

-i  -install           {package-name} - install the specified package(s)      - ex: asc -i bleachbit hexchat firefox
-s  -search            {search-term}  - search for the specified package(s)   - ex: asc -s bleachbit hexchat firefox
-u  -update            {package-name} - update the specified package(s)       - ex: asc -u bleachbit hexchat firefox
-r  -remove            {package-name} - remove the specified package(s)       - ex: asc -r bleachbit hexchat firefox
-il -install-local     {package-path} - install the local package via path    - ex: asc -il /home/user/package.t?z (g/x)
-lc -list-contents     {package-path} - safely list contents of local package - ex: asc -lc /home/user/package.t?z (g/x)
-sl -search-local      {search-term}  - search local packages for term        - ex: asc -sl mysql
-ld -list-dependencies {package-name} - list a package's dependencies         - ex: asc -ld mariadb

	Updating:

-ur       -update-repos      - update ALL repository file lists         - ex: asc -ur
-urslack  -update-slack-repo - update Slackware Package Repo lists ONLY - ex: asc -urslack
-ursbo    -update-sbo-repo   - update SlackBuilds Repository lists ONLY - ex: asc -ursbo
-up       -update-packages   - update all available packages            - ex: asc -up
-upslack  -update-slack      - update all Slackware Packages ONLY       - ex: asc -upslack
-upsbo    -update-sbo        - update all SlackBuilds ONLY              - ex: asc -upsbo

	Bulk Functions:

-lm  -list-mirrors           {  no input  } - list active mirrors                                 - ex: asc -lm
-ls  -list-installed         {  no input  } - list information on (all) installed packages (JSON) - ex: asc -ls
-lc  -list-categories        {  no input  } - lists all SlackBuild repository categories          - ex: asc -lc
-lcc -list-category-contents {  category  } - lists all packages in a SlackBuild category         - ex: asc -lcc Games

	Advanced:

-sf -show-files        {package-name} - list all files in installed package                       - ex: asc -sf mozilla-firefox
-do -disable-override  {package-name} - shows if a package is disabled or overridden (pkg vs sbo) - ex: asc -do mozilla-firefox
-sb -show-blacklist    {package-name} - shows if a package is on a blacklist (for updating)       - ex: asc -sb mozilla-firefox
-sq -show-queue        {package-name} - shows the (SlackBuild style) queue list for a package     - ex: asc -sq teminator



