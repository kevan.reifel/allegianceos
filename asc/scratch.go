package main

import (
	"fmt"
	"os"
	"strings"
)

func other() {
	CheckRoot()
	LoadConfiguration()
	CheckSlackPkgLock()
	ValidateFolderPaths()

	// Process Updates (packages true, slackbuilds true)
	//ProcessUpdates(true, true)

	// Process a package for queues and deps
	//ProcessPackage("virt-manager")

	// Process all programs in array
	//var Packages = []string{"bleachbit", "PyPanel", "urlscan"}
	//ProcessPackages(Packages)

	var Arguments = []string{
		"list-slackware - (list all installed Slackware packages and details) (Output: JSON)",
		"install <package_name> - (install the specified package) (Output: -j for JSON/default text)",
		"search <package_name> - (search for a Slackware package) (Output: JSON)",
		//"list-python - (list all installed Python 3 Modules and details) (Output: JSON)",
		//"list-gem - (list all Ruby Gem modules and details) (Output: JSON)",
		"	",
	}
	arg0 := strings.TrimSpace(os.Args[1])
	var fulfilled bool = bool(false)
	fmt.Println(arg0)
	switch arg0 {
	case "list-slackware":
		ListSlack()
		fulfilled = true
	case "install":
		ProcessPackages(os.Args[2:])
		fulfilled = true
	case "search":
		fulfilled = true

	}

	if !fulfilled {
		fmt.Println("\n=> Error:", arg0, "is not a valid argument.")
		fmt.Println("\nAvailable options:")
		for _, o := range Arguments {
			fmt.Println(o)
		}
	}
}
