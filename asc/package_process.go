package main

import (
	"fmt"
	"os"
)

func ProcessPackageLoop(pkg string) {
	packageName := CleanString(pkg)
	buildFound := SearchForOneSlackBuild(packageName)
	packageFound := SearchForOnePackage(packageName)
	var currentPackage PackageDetails
	currentPackage.Name = packageName
	overrideSlackBuild := false
	overridePackage := false

	if !buildFound && !packageFound {
		if !Config.ServerMode {
			fmt.Println("=> Warning!", packageName, "was not found, please check the name and try again.")
		}
		os.Exit(2)
	}
	if !buildFound && packageFound {
		if overridePackage {
			if !Config.ServerMode {
				if Config.Verbose {
					fmt.Println("=> Error! This package has been blacklisted!")
				}
				os.Exit(2)
			}
		} else {
			currentPackage.Source = "package"
			if !Config.ServerMode {
				if Config.Verbose {
					fmt.Println("=> Info: Package was found, no SlackBuild available.")
				}
			}
		}
	}
	if buildFound && !packageFound {
		if overrideSlackBuild {
			currentPackage.Source = ""
			if !Config.ServerMode {
				fmt.Println("=> Warning!", packageName, "has been blacklisted! Check for a blacklist file in /usr/share/aos if you feel this is wrong!")
				os.Exit(2)
			}
		} else {
			currentPackage.Source = "slackbuild"
			if !Config.ServerMode {
				if Config.Verbose {
					fmt.Println("=> Info: SlackBuild was found, no package available.")
				}
			}
			hasDeps, deps := GetDepFile(packageName)
			if hasDeps {
				if !Config.ServerMode {
					if Config.Verbose {
						fmt.Println("=> Info: SlackBuild for ", pkg, "has dependencies, processing..")
					}
				}
				State.Current = append(State.Current, currentPackage)
				for i := 0; i < len(deps); i++ {
					if i != len(deps)-1 {
						ProcessPackageLoop(deps[i])
					}
				}
			} else {
				if !Config.ServerMode {
					if Config.Verbose {
						fmt.Println("=> Info: SlackBuild for ", pkg, "has no dependencies, proceeding..")
					}
				}
				State.Current = append(State.Current, currentPackage)
			}
		}
	}
	if buildFound && packageFound {
		if Config.DominateInstaller == "slackbuild" {
			currentPackage.Source = "slackbuild"
		}
		if Config.DominateInstaller == "package" {
			currentPackage.Source = "package"
		}
		if overridePackage && !overrideSlackBuild {
			currentPackage.Source = "slackbuild"
			if !Config.ServerMode {
				if Config.Verbose {
					fmt.Println("=> Warning! This package has been blacklisted, using a SlackBuild instead..")
				}
			}
		}
		if !overridePackage && overrideSlackBuild {
			currentPackage.Source = "package"
			if !Config.ServerMode {
				if Config.Verbose {
					fmt.Println("=> Warning! This SlackBuild has been blacklisted, using a package instead..")
				}
			}
		}
		if overridePackage && overrideSlackBuild {
			if !Config.ServerMode {
				if Config.Verbose {
					fmt.Println("=> Error! Both the package and SlackBuild for the requested process have been blacklisted!")
				}
				os.Exit(2)
			}
		}
		if currentPackage.Source == "slackbuild" {
			hasDeps, deps := GetDepFile(packageName)
			if hasDeps {
				if !Config.ServerMode {
					if Config.Verbose {
						fmt.Println("=> Info: SlackBuild for ", pkg, "has dependencies, processing..")
					}
				}
				State.Current = append(State.Current, currentPackage)
				for i := 0; i < len(deps); i++ {
					if i != len(deps)-1 {
						if !Config.ServerMode {
							if Config.Verbose {
								fmt.Println("=> Info: ", deps[i], " was found to be a dependency of ", pkg, ", processing..")
							}
						}
						ProcessPackageLoop(deps[i])
					}
				}
			} else {
				if !Config.ServerMode {
					if Config.Verbose {
						fmt.Println("=> Info: SlackBuild for ", pkg, "has no dependencies, proceeding..")
					}
				}
				State.Current = append(State.Current, currentPackage)
			}
		}
		if currentPackage.Source == "package" {
			if !Config.ServerMode {
				fmt.Println("unhandled.")
			}
		}
	}
}

func ProcessPackage(pkg string) {
	ProcessPackageLoop(pkg)
	if Config.Verbose {
		fmt.Println("Current:", State.Current)
	}
}

func ProcessPackages(pkgs []string) {
	for i := 0; i < len(pkgs); i++ {
		ProcessPackageLoop(pkgs[i])
	}
	if Config.Verbose {
		fmt.Println(State.Current)
	}
}

func CountPackageOccurences(packages []PackageDetails, pkg string) int {
	counter := 0
	for i := 0; i < len(packages); i++ {
		if packages[i].Name == pkg {
			counter++
		}
	}
	return counter
}
