package main

func CollapseCurrent() {
	currentList := State.Current
	packageNames := GetPackageNames(currentList)
	sorted := currentList
	for i := 0; i < len(packageNames); i++ {
		occurences := CountPackageOccurences(currentList, packageNames[i])
		if occurences > 1 {
			for o := 1; o < occurences; o++ {
				sorted = RemovePackageByName(sorted, packageNames[i])
			}
		}
	}
	State.Current = sorted
}

func CollapseUpgrades() {
	currentList := UpgradeResponse.Upgrades
	packageNames := GetPackageNames(currentList)
	sorted := currentList
	for i := 0; i < len(packageNames); i++ {
		occurences := CountPackageOccurences(currentList, packageNames[i])
		if occurences > 1 {
			for o := 1; o < occurences; o++ {
				sorted = RemovePackageByName(sorted, packageNames[i])
			}
		}
	}
	UpgradeResponse.Upgrades = sorted
}

func CollapseRebuilds() {
	currentList := UpgradeResponse.Rebuilds
	packageNames := GetPackageNames(currentList)
	sorted := currentList
	for i := 0; i < len(packageNames); i++ {
		occurences := CountPackageOccurences(currentList, packageNames[i])
		if occurences > 1 {
			for o := 1; o < occurences; o++ {
				sorted = RemovePackageByName(sorted, packageNames[i])
			}
		}
	}
	UpgradeResponse.Rebuilds = sorted
}

func CollapseRemovals() {
	currentList := UpgradeResponse.Removals
	packageNames := GetPackageNames(currentList)
	sorted := currentList
	for i := 0; i < len(packageNames); i++ {
		occurences := CountPackageOccurences(currentList, packageNames[i])
		if occurences > 1 {
			for o := 1; o < occurences; o++ {
				sorted = RemovePackageByName(sorted, packageNames[i])
			}
		}
	}
	UpgradeResponse.Removals = sorted
}

func CollapseAdditions() {
	currentList := UpgradeResponse.Additions
	packageNames := GetPackageNames(currentList)
	sorted := currentList
	for i := 0; i < len(packageNames); i++ {
		occurences := CountPackageOccurences(currentList, packageNames[i])
		if occurences > 1 {
			for o := 1; o < occurences; o++ {
				sorted = RemovePackageByName(sorted, packageNames[i])
			}
		}
	}
	UpgradeResponse.Additions = sorted
}

func CollapseDowngrades() {
	currentList := UpgradeResponse.Downgrades
	packageNames := GetPackageNames(currentList)
	sorted := currentList
	for i := 0; i < len(packageNames); i++ {
		occurences := CountPackageOccurences(currentList, packageNames[i])
		if occurences > 1 {
			for o := 1; o < occurences; o++ {
				sorted = RemovePackageByName(sorted, packageNames[i])
			}
		}
	}
	UpgradeResponse.Downgrades = sorted
}
