package main

//"io"
//"runtime"
import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

var Config Configuration
var DisallowedCharacters []string = []string{"%", "$", "*", "(", ")", "^", "!", "|", "&"}
var IgnoredDirectories []string = []string{"isolinux/", "kernels/", "usb-and-pxe-installers/", "patches/", "testing/", "pasture/", "EFI/"}
var ChangeLogs []ChangelogData
var UpdatedChangeLogs []ChangelogData
var State StateObject
var UpgradeResponse PackageChangeResponse

func InstallSlackBuild(packageName string) {
	command := "sbopkg -i" + packageName
	output := strings.TrimSpace(RunCommand(command))
	if !Config.ServerMode {
		fmt.Println("-> Install completed for:" + packageName)
	}
	logTime := strconv.FormatInt(time.Now().Unix(), 10)
	filename := Config.PathToInstallLogs + "sbo_install_" + packageName + "_" + logTime + ".log"
	CreateFile(filename, output)
	if !Config.ServerMode {
		if Config.Verbose {
			fmt.Println(output)
		}
	}
}

func InstallPackage(packageName string) {
	command := "slapt-get -i" + packageName
	output := strings.TrimSpace(RunCommand(command))
	if !Config.ServerMode {
		fmt.Println("-> Install completed for:" + packageName)
	}
	logTime := strconv.FormatInt(time.Now().Unix(), 10)
	filename := Config.PathToInstallLogs + "pkg_install_" + packageName + "_" + logTime + ".log"
	CreateFile(filename, output)
}

func InstallLocalPackage(packagePath string, packageName string) {
	command := "upgradepkg --install-new" + packagePath
	output := strings.TrimSpace(RunCommand(command))
	if !Config.ServerMode {
		fmt.Println("-> Install completed for:" + packageName)
	}
	logTime := strconv.FormatInt(time.Now().Unix(), 10)
	filename := Config.PathToInstallLogs + "pkg_install_" + packageName + "_" + logTime + ".log"
	CreateFile(filename, output)
}

func RemoveLocalPackage(packagePath string, packageName string) {
	command := "removepkg" + packagePath
	output := strings.TrimSpace(RunCommand(command))
	if !Config.ServerMode {
		fmt.Println("-> Removal completed for:" + packageName)
	}
	logTime := strconv.FormatInt(time.Now().Unix(), 10)
	filename := Config.PathToInstallLogs + "pkg_removal_" + packageName + "_" + logTime + ".log"
	CreateFile(filename, output)
}

func RemovePackages(list []PackageDetails) {
	if len(State.Installed) == 0 {
		ProcessInstalledPackages()
	}
	for i := 0; i < len(list); i++ {
		packageDetails, _ := GetInstalledPackage(list[i].Name)
		packagePath := packageDetails.PackagePath
		RemoveLocalPackage(packagePath, list[i].Name)
	}
}

func UpgradeLocalPackage(packagePath string, force bool) {
	if len(State.Installed) == 0 {
		ProcessInstalledPackages()
	}
	packageDetails := StringToPackageDetails(packagePath, "Installed.")
	packageName := packageDetails.Name
	if force {
		RemoveLocalPackage(packagePath, packageName)
	}
	InstallLocalPackage(packageName, packageName)
}

func UpgradePackage(packageName string, force bool) {
	if len(State.Installed) == 0 {
		ProcessInstalledPackages()
	}
	packageDetails, _ := GetInstalledPackage(packageName)
	packagePath := packageDetails.PackagePath
	if force {
		RemoveLocalPackage(packagePath, packageName)
	}
	InstallPackage(packageName)
}

func UpgradePackages(list []PackageDetails) {
	State.RunningPackageUpdates = true
	State.RunningSBOUpdates = true
	State.CurrentQueue = list
	total := len(list)
	for i := 0; i < total; i++ {
		pkg := list[i]
		State.CurrentPackage = pkg
		if pkg.Source == "package" {
			UpgradePackage(pkg.Name, false)
		}
		if pkg.Source == "slackbuild" {
			InstallSlackBuild(pkg.Name)
		}
		State.CompletedPackages = append(State.CompletedPackages, pkg)
	}
	var empty PackageDetails
	State.CurrentPackage = empty
	State.RunningPackageUpdates = false
	State.RunningSBOUpdates = false
}

func DowngradePackages(list []PackageDetails) {
	State.RunningPackageUpdates = true
	State.RunningSBOUpdates = true
	State.CurrentQueue = list
	total := len(list)
	for i := 0; i < total; i++ {
		pkg := list[i]
		State.CurrentPackage = pkg
		if pkg.Source == "package" {
			UpgradePackage(pkg.Name, true)
		}
		if pkg.Source == "slackbuild" {
			InstallSlackBuild(pkg.Name)
		}
		State.CompletedPackages = append(State.CompletedPackages, pkg)
	}
	var empty PackageDetails
	State.CurrentPackage = empty
	State.RunningPackageUpdates = false
	State.RunningSBOUpdates = false
}

func InstallPackages(list []PackageDetails) {
	State.RunningInstalls = true
	State.CurrentQueue = list
	total := len(list)
	for i := 0; i < total; i++ {
		pkg := list[i]
		State.CurrentPackage = pkg
		if pkg.Source == "package" {
			InstallPackage(pkg.Name)
		}
		if pkg.Source == "slackbuild" {
			InstallSlackBuild(pkg.Name)
		}
		State.CompletedPackages = append(State.CompletedPackages, pkg)
	}
	var empty PackageDetails
	State.CurrentPackage = empty
	State.RunningInstalls = false
}
