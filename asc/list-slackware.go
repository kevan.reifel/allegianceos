package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"strings"
	//"io"
	//"runtime"
)

type InstalledPackage struct {
	FullPath     string   `json:"fullPath"`
	FullName     string   `json:"fullName"`
	Name         string   `json:"name"`
	Arch         string   `json:"arch"`
	Version      string   `json:"version"`
	Build        string   `json:"build"`
	UnCompressed string   `json:"uncompressed size"`
	Compressed   string   `json:"compressed size"`
	Source       string   `json:"source"`
	Location     string   `json:"location"`
	SmallDesc    string   `json:"smdesc"`
	FullDesc     string   `json:"fulldesc"`
	HomePage     string   `json:"homepage"`
	FileList     []string `json:"file_list"`
	Bit64        bool     `json:"64-bit"`
}

func ListSlack() {
	cmdName := "ls /var/log/packages"
	cmdArgs := strings.Fields(cmdName)

	cmd := exec.Command(cmdArgs[0], cmdArgs[1:]...)
	stdout, _ := cmd.StdoutPipe()
	cmd.Start()
	r := bufio.NewReader(stdout)

	var PackageList []string
	for {
		line, _, _ := r.ReadLine()

		if string(line) != "" {
			var unCompressedSize string
			var compressedSize string
			var packageLocation string
			var smallDescription string
			var fullDescription string
			var homePage string
			var fileList []string
			var compat32 bool = bool(false)
			FoundDescription := false
			FoundFileList := false
			str := strings.Split(string(line), "-")
			if len(str) >= 4 {
				build := str[len(str)-1]
				arch := str[len(str)-2]
				version := str[len(str)-3]
				name := str[:len(str)-3]
				fname := ""
				for i, n := range name {
					if i == (len(name) - 1) {
						fname = fname + n
					} else {
						fname = fname + n + "-"
					}
				}
				if strings.Contains(fname, "compat32") {
					compat32 = true
				}

				file, err := os.Open("/var/log/packages/" + string(line))
				if err != nil {
					fmt.Println(err)
				}
				defer file.Close()

				scanner := bufio.NewScanner(file)
				for scanner.Scan() {
					if strings.Contains(scanner.Text(), "COMPRESSED PACKAGE SIZE:") {
						if strings.Contains(scanner.Text(), "UNCOMPRESSED") {
							ucstr := strings.Split(scanner.Text(), ":")
							ucsize := strings.TrimSpace(ucstr[len(ucstr)-1])
							unCompressedSize = ucsize
						} else {
							cstr := strings.Split(scanner.Text(), ":")
							csize := strings.TrimSpace(cstr[len(cstr)-1])
							compressedSize = csize
						}

					}
					if strings.Contains(scanner.Text(), "PACKAGE LOCATION") {
						ploc := strings.Split(scanner.Text(), ":")
						location := strings.TrimSpace(ploc[len(ploc)-1])
						packageLocation = location
					}
					if strings.Contains(scanner.Text(), "PACKAGE DESCRIPTION") {
						FoundDescription = true
					}
					if strings.Contains(scanner.Text(), "FILE LIST") {
						FoundFileList = true
					}
					if FoundDescription && !FoundFileList {
						if !strings.Contains(scanner.Text(), "PACKAGE DESCRIPTION") {
							adesc := strings.Split(scanner.Text(), fname+":")
							aline := strings.TrimSpace(adesc[len(adesc)-1])

							if strings.Contains(scanner.Text(), fname+" (") {
								sdesc := strings.Split(scanner.Text(), fname+" (")
								sline := strings.Replace(strings.TrimSpace(sdesc[len(sdesc)-1]), ")", "", 1)
								smallDescription = sline
							} else {
								if (!strings.Contains(scanner.Text(), "Homepage:")) && (!strings.Contains(scanner.Text(), "Webpage:")) {
									fullDescription = fullDescription + aline + " "
								}
							}

							if (strings.Contains(scanner.Text(), "Homepage:")) || (strings.Contains(scanner.Text(), "Webpage:")) {
								var prelim string
								if strings.Contains(scanner.Text(), "Homepage:") {
									prelim = strings.TrimSpace(strings.Replace(aline, "Homepage:", "", 1))
								}
								if strings.Contains(scanner.Text(), "Webpage:") {
									prelim = strings.TrimSpace(strings.Replace(aline, "Webpage:", "", 1))
								}
								homePage = prelim
							}

						}
					}
					if FoundFileList {
						fileList = append(fileList, strings.TrimSpace(scanner.Text()))
					}
				}

				if err := scanner.Err(); err != nil {
					fmt.Println(err)
				}
				file.Close()
				packageA := InstalledPackage{
					FullPath:     "/var/log/packages/" + string(line),
					FullName:     string(line),
					Name:         fname,
					Arch:         arch,
					Version:      version,
					Build:        build,
					UnCompressed: unCompressedSize,
					Compressed:   compressedSize,
					Source:       "Slackware Package",
					Location:     packageLocation,
					SmallDesc:    strings.TrimSpace(smallDescription),
					FullDesc:     strings.TrimSpace(fullDescription),
					HomePage:     homePage,
					FileList:     fileList,
					Bit64:        !compat32,
				}

				p, err := json.Marshal(packageA)
				if err != nil {
					fmt.Println(err)
				}

				PackageList = append(PackageList, string(p))
			}
		} else {
			break
		}
	}
	fmt.Println(PackageList)
	cmd.Wait()
}
