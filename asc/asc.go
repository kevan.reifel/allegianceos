package main

import (
	"fmt"
	"os"
	"strings"
)

func listMore() {
	var MoreArguments = []string{
		"\n	Bulk Functions:\n",
		"-lm  -list-mirrors           {  no input  } - list active mirrors                                 - ex: asc -lm",
		"-ls  -list-installed         {  no input  } - list information on (all) installed packages (JSON) - ex: asc -ls",
		"-lc  -list-categories        {  no input  } - lists all SlackBuild repository categories          - ex: asc -lc",
		"-lcc -list-category-contents {  category  } - lists all packages in a SlackBuild category         - ex: asc -lcc Games\n",
		"	Advanced:\n",
		"-sf -show-files        {package-name} - list all files in installed package                       - ex: asc -sf mozilla-firefox",
		"-do -disable-override  {package-name} - shows if a package is disabled or overridden (pkg vs sbo) - ex: asc -do mozilla-firefox",
		"-sb -show-blacklist    {package-name} - shows if a package is on a blacklist (for updating)       - ex: asc -sb mozilla-firefox",
		"-sq -show-queue        {package-name} - shows the (SlackBuild style) queue list for a package     - ex: asc -sq teminator",
		"\n",
	}
	for _, o := range MoreArguments {
		fmt.Println(o)
	}
}

func search() {
	number := len(os.Args)
	if number < 3 {
		fmt.Println("Error: unable to continue. Must provide a search term.")
		os.Exit(2)
	}
	packageCount, packageResults := SearchSlackPackage(os.Args[2])
	slackBuildCount, slackBuildResults := SearchSlackBuild(os.Args[2])
	pkgHeader := fmt.Sprintf("\n #=#=#=# Slackware Packages: %d result(s) found.\n\n --- --- ---", packageCount)
	fmt.Println(pkgHeader)
	for i := 0; i < packageCount; i++ {
		PackagePrinter(packageResults[i])
	}
	fmt.Println(" --- --- ---")
	sboHeader := fmt.Sprintf("\n\n #=#=#=# SlackBuilds: %d result(s) found.\n\n --- --- ---", slackBuildCount)
	fmt.Println(sboHeader)
	for j := 0; j < slackBuildCount; j++ {
		PackagePrinter(slackBuildResults[j])
	}
	fmt.Println(" --- --- ---")
}

func install() {
	number := len(os.Args)
	if number < 3 {
		fmt.Println("Error: unable to continue. Please provide at least one package name.")
		os.Exit(2)
	}
	pkgLock := CheckSlackPkgLock()
	sboLock := CheckSBOLock()
	if sboLock || pkgLock {
		fmt.Println("Error: unable to continue. Removal of SBO and/or SlackPkg lock file failed.")
		os.Exit(2)
	}
	ProcessPackages(os.Args[2:])
	InstallPackages(State.Current)
}
func upgradePackages() {
	pkgLock := CheckSlackPkgLock()
	sboLock := CheckSBOLock()
	if sboLock || pkgLock {
		fmt.Println("Error: unable to continue. Removal of SBO and/or SlackPkg lock file failed.")
		os.Exit(2)
	}
	ProcessUpdates(true, true)
	InstallUpdates()
}

func upgradeSlackware() {
	pkgLock := CheckSlackPkgLock()
	if pkgLock {
		fmt.Println("Error: unable to continue. Removal of SlackPkg lock file failed.")
		os.Exit(2)
	}
	ProcessUpdates(true, false)
	InstallUpdates()
	CheckSlackPkgLock()
}

func upgradeSlackBuilds() {
	sboLock := CheckSBOLock()
	if sboLock {
		fmt.Println("Error: unable to continue. Removal of SBO lock file failed.")
		os.Exit(2)
	}
	ProcessUpdates(false, true)
	InstallUpdates()
}

func main() {
	CheckRoot()
	LoadConfiguration()
	ValidateFolderPaths()
	var Arguments = []string{
		"\n	Package Management:\n",
		"-i  -install           {package-name} - install the specified package(s)      - ex: asc -i bleachbit hexchat firefox",
		// -s is python3 based
		"-s  -search            {search-term}  - search for the specified package(s)   - ex: asc -s bleachbit hexchat firefox",
		"-u  -update            {package-name} - update the specified package(s)       - ex: asc -u bleachbit hexchat firefox",
		"-r  -remove            {package-name} - remove the specified package(s)       - ex: asc -r bleachbit hexchat firefox",
		"-il -install-local     {package-path} - install the local package via path    - ex: asc -il /home/user/package.t?z (g/x)",
		"-lc -list-contents     {package-path} - safely list contents of local package - ex: asc -lc /home/user/package.t?z (g/x)",
		"-sl -search-local      {search-term}  - search local packages for term        - ex: asc -sl mysql",
		"-ld -list-dependencies {package-name} - list a package's dependencies         - ex: asc -ld mariadb\n",
		"	Updating:\n",
		"-ur       -update-repos      - update ALL repository file lists         - ex: asc -ur",
		"-urslack  -update-slack-repo - update Slackware Package Repo lists ONLY - ex: asc -urslack",
		"-ursbo    -update-sbo-repo   - update SlackBuilds Repository lists ONLY - ex: asc -ursbo",
		"-up       -update-packages   - update all available packages            - ex: asc -up",
		"-upslack  -update-slack      - update all Slackware Packages ONLY       - ex: asc -upslack",
		"-upsbo    -update-sbo        - update all SlackBuilds ONLY              - ex: asc -upsbo\n",
		"	Need more options? Try 'asc -mo'.",
		"\n",
	}
	arg0 := strings.TrimSpace(os.Args[1])
	var fulfilled bool = bool(true)
	switch arg0 {
	case "-mo":
		listMore()
	case "-i":
		install()
	case "-install":
		install()
	case "-s":
		search()
	case "-search":
		search()
	case "-up":
		upgradePackages()
	case "-update-packages":
		upgradePackages()
	case "-upslack":
		upgradeSlackware()
	case "-update-slack":
		upgradeSlackware()
	case "-upsbo":
		upgradeSlackBuilds()
	case "-update-sbo":
		upgradeSlackBuilds()
	default:
		fulfilled = false
	}

	if !fulfilled {
		fmt.Println("Error:", arg0, "is not a valid (or available) argument. See options below.")
		for _, o := range Arguments {
			fmt.Println(o)
		}
		os.Exit(2)
	}
}
