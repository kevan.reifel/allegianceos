#!/usr/bin/env python3
#
#	A repository updater for AllegianceOS
#	Written by Kevan Reifel
#   License: Apache 2.0
#
###########################################
import subprocess
import os
import sys
from datetime import date

# Change this (integer) to reflect how many days old the repos must be before rerunning. 
# Default is 1.
DAYS=1

# Tell the user what we're doing:
print("Running sync across repos...")

# Set some flags:
argument=None
update_file=False

# Get today's date:
today = str(date.today()).replace("-","")

# Open the last_full_sync file:
syncfile=open("/usr/share/aos/last_full_sync",'r')

# Test to see if the user issued a "force":
try:
	argument=sys.argv[1]
	if argument=="-f":
		update_file=True
		
except IndexError:
	pass
	

# If the update flag is default:
if update_file==False:	
	for line in syncfile:
		# If the file was empty:
		# (note.. this may not be working to detect "empty" correctly..)
		# If you wiped the file and are having an issue, just recreate it with a datestring, i.e.: 20200101
		if line.strip()=="":
			print("never")
			update_file=True	
		# If not empty (no issues here..):	
		else:
			last=line.strip()
			now=int(today)
			then=int(last)
			
			# If the last sync was more than the configured amount, update:
			if (now-then)>=DAYS:
				print("More than one day old..")
				update_file=True
				
			# If it's the same day, exit:
			if now==then:
				print("Repos were updated today, exiting.\n(Note: use the -f option to force!)")
				sys.exit()
				
			# If it looks like it was synced in the future, just warn and rerun:
			if (now-then)<0:
				print("Last sync was in the future.. did you change your timezone..?")
				update_file=True

	# Close the file
	syncfile.close()	
	
# If the update_file flag was set:
if update_file==True:
		
	# Update the slackpkg gpg keys:
	gpg_update = subprocess.Popen(["slackpkg","update","gpg"], stdout=subprocess.PIPE, universal_newlines=True)
	for line in iter(gpg_update.stdout.readline, ""):
		print(line.strip())
	
	# Update the slackpkg repo list:
	slackware_update = subprocess.Popen(["slackpkg","update"], stdout=subprocess.PIPE, universal_newlines=True)
	for line in iter(slackware_update.stdout.readline, ""):
		print(line.strip())
	
	# Update slapt's repo list:	
	slapt_update = subprocess.Popen(["slapt-get","-u"], stdout=subprocess.PIPE, universal_newlines=True)
	for line in iter(slapt_update.stdout.readline, ""):
		print(line.strip())
	
	# Update sbopkg's SlackBuilds:
	sbo_update = subprocess.Popen(["sbopkg","-r"], stdout=subprocess.PIPE, universal_newlines=True)
	for line in iter(sbo_update.stdout.readline, ""):
		print(line.strip())
	
	# Open the file:
	syncf=open("/usr/share/aos/last_full_sync","w+")
	
	# Write in today's date:
	syncf.write(today)
	
	# Close the file:
	syncf.close()

# Exit.. (just in case that wasn't implied..)
sys.exit()
