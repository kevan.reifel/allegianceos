These are the python3 scripts currently bundled with AOS.

They are slowly being converted to Golang, but most are still being called from asc. 

Note: asc combines these features into one binary. See the "asc" folder for the Go code.

In order for these to work, copy them to your /usr/sbin/ directory (root only) and make them executable. 

Slackware 15.0 is required. 

For full functionality, you'll need sbopkg, slapt-get and slackpkg (standard) installed.
 


Slackware is a registered trademark of Patrick Volkerding.
No partnership written or otherwise exists between this project and Slackware.
All software comes as-is, use at your own risk.

